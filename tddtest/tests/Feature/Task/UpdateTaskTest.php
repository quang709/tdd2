<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    public function getUpdateTaskRoute($id)
    {
        return route('tasks.update', $id);
    }

    public function getRedirectTaskRoute()
    {
        return route('tasks.index');
    }

    /** @test */

    public function user_can_update_task()
    {
        $task = Task::factory()->create();

        $data = ['name' => $this->faker->name, 'content' => $this->faker->text];
        $response = $this->put($this->getUpdateTaskRoute($task->id), $data);
        $this->assertDatabaseHas('tasks', $data);
        $taskCheck = Task::find($task->id);
        $this->assertSame($taskCheck->name, $data['name']);
        $this->assertSame($taskCheck->content, $data['content']);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHas(['name','content']);
        $response->assertRedirect($this->getRedirectTaskRoute());

    }

    /** @test */

    public function user_can_not_update_task_if_name_is_null()
    {
        $task = Task::factory()->create();

        $data = ['name' => '', 'content' => $this->faker->text];
        $response = $this->put($this->getUpdateTaskRoute($task->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);


    }

    /** @test */

    public function user_can_not_update_task_if_content_is_null()
    {
        $task = Task::factory()->create();

        $data = ['name' => $this->faker->name, 'content' => ''];
        $response = $this->put($this->getUpdateTaskRoute($task->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['content']);


    }

    /** @test */

    public function user_can_not_update_task_if_data_is_null()
    {
        $task = Task::factory()->create();

        $data = ['name' => '', 'content' => ''];
        $response = $this->put($this->getUpdateTaskRoute($task->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['content', 'name']);


    }

    /** @test */

    public function user_can_not_update_task_if_task_is_not_exist()
    {
        $id = -1;
        $response = $this->put($this->getUpdateTaskRoute($id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);


    }
}
