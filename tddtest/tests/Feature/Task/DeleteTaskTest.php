<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    public function getDeleteTaskRoute($id)
    {
        return route('tasks.destroy', $id);
    }

    public function getRedirectTaskRoute()
    {
        return route('tasks.index');
    }

    /** @test */

    public function user_can_delete_task()
    {
        $task = Task::factory()->create();
        $countTaskBefore = Task::count();
        $response = $this->delete($this->getDeleteTaskRoute($task->id));
        $countTaskAfter = Task::count();
        $this->assertEquals($countTaskBefore - 1, $countTaskAfter);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('tasks', $task->toArray());
        dd($task->toArray());
        $response->assertRedirect($this->getRedirectTaskRoute());

    }
    /** @test */

    public function user_can_not_delete_tasks()
    {
        $id = -1;

        $response = $this->delete($this->getDeleteTaskRoute($id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}
