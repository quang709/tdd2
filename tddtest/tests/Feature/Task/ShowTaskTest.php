<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowTaskTest extends TestCase
{
    public function getShowTaskTest($id)
    {
        return route('tasks.show',$id);
    }

    /** @test  */

    public function user_can_show_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getShowTaskTest($task->id));
        $response->assertViewIs('tasks.show');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($task->name);


    }


}
